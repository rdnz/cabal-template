{
  description = "haskell shell";

  # inputs.nixpkgs.url = "github:nixos/nixpkgs/4a5241e7522493cb13011fb3a27a9b6d4228a229"; # nixos-unstable-2023-10-04
  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem
      (system:
        with nixpkgs.legacyPackages.${system};
        {
          devShells.default =
            mkShellNoCC {
              packages =
                [
                  ghc
                  cabal-install
                  haskellPackages.cabal-fmt
                  ghcid
                  # haskell-language-server
                ];
            };
        }
      );
}
