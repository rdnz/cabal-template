{-# options_ghc
  -Wno-redundant-constraints
#-}

module Partial where

import GHC.Exts (RuntimeRep, TYPE)
import Prelude qualified as P
import Relude qualified as R
import Unsafe.Coerce (unsafeCoerce)
import Relude hiding (error)

type Partial = (Partial', HasCallStack)

unsafePartial :: ((Partial') => a) -> a
unsafePartial a =
  unsafeCoerce (Wrap a) (P.error "impossible. `Partial'` is empty.")

error :: forall (r :: RuntimeRep) (a :: TYPE r). (Partial) => Text -> a
error = R.error

class Partial'
newtype Wrap a = Wrap (Partial' => a)
